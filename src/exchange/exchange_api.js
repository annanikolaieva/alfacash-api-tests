const axios = require('axios');

let pathList = {
    getExchangeLimits: "/api/limit/",
    getExchangePairLimit: "/api/limit/",
    getCurrenciesList: "/api/getcoins",
    postOrder: "/api/create.json",
    postRate: "/api/rate.json",
    postAddress: "/api/validateaddress.json",
    getOrderStatus: "/api/status/"
}

class APIExchange {
    constructor(baseURL) {
        this.baseURL = baseURL
    }

    headers() {
        return {
            "accept": "application/json",
            "content-type": "application/json",
        }
    }

    getExchangeLimits(gate_deposit, gate_withdrawal) {
        return axios.get(
            this.baseURL + pathList.getExchangeLimits + gate_deposit + '/' + gate_withdrawal,
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    getExchangePairLimit(pair) {
        return axios.get(
            this.baseURL + pathList.getExchangePairLimit + pair,
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    getCurrenciesList() {
        return axios.get(
            this.baseURL + pathList.getCurrenciesList,
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    postOrder(order) {
        return axios.post(
            this.baseURL + pathList.postOrder,
            {
                gate_deposit: order.gate_deposit,
                gate_withdrawal: order.gate_withdrawal,
                pair: order.pair,
                deposit_amount: order.deposit_amount,
                withdrawal_amount: order.withdrawal_amount,
                email: order.email,
                options: order.options
            },
            {headers: this.headers()})
            // .then(resp => {
            //     return resp.data
            // })
    }

    postRate(order) {
        return axios.post(
            this.baseURL + pathList.postRate,
            {
                gate_deposit: order.gate_deposit,
                gate_withdrawal: order.gate_withdrawal,
                pair: order.pair,
            },
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    postAddress(order) {
        return axios.post(
            this.baseURL + pathList.postAddress,
            {
                gate_withdrawal: order.gate_withdrawal,
                currency: order.currency,
                options: order.options,
            },
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    getOrderStatus(secret_key) {
        return axios.get(
            this.baseURL + pathList.getOrderStatus + secret_key,
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }
}

module.exports.APIExchange = APIExchange
