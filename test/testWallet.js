let assert = require('chai').assert;

let env = require('../private/walletapp/env')
let api = require('../src/walletapp/auth')
let login = require('../private/walletapp/login')
let mnemonicUtils = require ('../src/walletapp/createWallet')


describe('AlfaCash', () => {
    let client = new api.APIAuth(env.url)


    it('should login', async () => {
        try {
            let loginInfo = await client.postLogin(
                {
                    address: login.address,
                    signature: login.signature,
                }
            );
            console.log('loginInfo:',loginInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should login check', async () => {
        try {
            let loginInfo = await client.postLoginCheck(
                { login:login.login}
            );
            console.log(loginInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should register', async () => {
        try {
            let mnemonic = mnemonicUtils.generateMnemonic()
            console.log("mnemonic:",mnemonic)

            let regData = mnemonicUtils.getRegistrationData(mnemonic)
            console.log("regData:",regData)
            let loginInfo = await client.postRegistration(regData);
            console.log(loginInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });


    it('should show profile', async () => {
        try {
            let loginInfo = await client.postLogin(
                {
                    address: login.address,
                    signature: login.signature,
                }
            );
            console.log(loginInfo)

            console.log("headers: ", client.headers())
            let profileInfo = await client.getProfile();
            console.log(profileInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });


})
