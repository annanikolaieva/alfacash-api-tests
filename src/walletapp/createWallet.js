const Mnemonic = require ('bitcore-mnemonic')
const bitcore= require('bitcore-lib')
const bip39 = require('bip39')

function generateMnemonic() {
    let mnemonic = new Mnemonic()
    return mnemonic.phrase
}

function mnemonicToSeed(mnemonic) {
    const seed = bip39.mnemonicToSeedSync(mnemonic)
    return seed.toString('hex')
}


function getRegistrationData (mnemonic) {

    const MESSAGE_TO_SIGN = 'register'

    let masterSeed = mnemonicToSeed(mnemonic)
    let hdKey = bitcore.HDPrivateKey.fromSeed(masterSeed)
    let msg = new bitcore.Message(MESSAGE_TO_SIGN)
    let signature = msg.sign(hdKey.privateKey)
    let address = hdKey.publicKey.toAddress().toString()
    return {address, signature}
}

module.exports ={
    generateMnemonic,
    mnemonicToSeed,
    getRegistrationData
}
