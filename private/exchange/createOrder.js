let names = {
    btc: {name: "bitcoin", symbol: "BTC", address: "mhYAqiLCxY1zokBvJTZyfodgC5mrM8keMM"},
    ltc: {name: "litecoin", symbol: "LTC", address: "n4dp5LRD7J2A2yRAmhH9pXUCiSYGwDXfL7"},
    bch: {name: "bitcoincash", symbol: "BCH", address: "mvyj1m1nA1PLzvq6E8ZLEHGG61aUrx9UDx"},
    eth: {name: "ethereum", symbol: "ETH", address: "0x84bcd864cbcad6db770d34c8ba59532a0e4b88b2"},
    // xlm: {name: "stellar", symbol: "XLM", address:"GBQBWSOFRCCIOOVQUOGRX65L2RAZQ473QS6ERLVMQVRVCDGQEST3525N"},
    //xrp: {name: "ripple", symbol: "XRP", address: "rDGXW5J5VFrSM1U4iWYYenfLUCzPbUVUaD"},
    // eos: {name: "eos", symbol: "EOS", address:"EOS5zJ61DaRrEQE5yzxLmTuESQxmDM3DStByPKcVZsjR7w2b78ApZ"},
}

function exchangePair() {
    let keys = Object.keys(names)

    let from = names[keys[keys.length * Math.random() << 0]]
    let to = names[keys[keys.length * Math.random() << 0]]

    if (to === from) {
        to = names[keys[keys.length * Math.random() << 0]]
    }
    return {from, to}
}


module.exports = {
    btc: names.btc,
    ltc: names.ltc,
    bch: names.bch,
    eth: names.eth,
    // xlm: names.xlm,
    xrp: names.xrp,
    // eos: names.eos,
    deposit_amount: "0.005",
    withdrawal_amount: "0.018",
    email: "anatolii.kobzar@inn4science.com",
    currency: "LTC",
    exchangePair
}
