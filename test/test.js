let assert = require('chai').assert;

let env = require('../private/env')
let api = require('../src/affiliate/api')
let paymentOrder = require('../private/affiliate/paymentCreate')


describe('affiliate', () => {
    let client = new api.APIClient(env.URL, env.TOKEN)

    it('should get affiliate info', async () => {
        try {
            let affiliateInfo = await client.getAffiliate();
            console.log(affiliateInfo)
            assert.isTrue(affiliateInfo.hasOwnProperty('id'))
            assert.isTrue(affiliateInfo.hasOwnProperty('link'))
            assert.isTrue(affiliateInfo.hasOwnProperty('balance'))
            assert.isTrue(affiliateInfo.hasOwnProperty('level'))
            assert.isTrue(affiliateInfo.hasOwnProperty('earned'))
            assert.isTrue(affiliateInfo.hasOwnProperty('attracted_users'))

        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get affiliate levels', async () => {
        try {
            let affiliateInfo = await client.getAffiliateLevels();
            console.log(affiliateInfo)

        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get referrals', async () => {
        try {
            let affiliateInfo = await client.getAffiliateReferrals();
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get referrals income', async () => {
        try {
            let affiliateInfo = await client.getAffiliateIncome();
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get referrals payments', async () => {
        try {
            let affiliateInfo = await client.getAffiliatePayment();
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get referrals payments by id', async () => {
        try {
            let affiliateInfo = await client.getAffiliatePayment();
            console.log(affiliateInfo)
            let pId = affiliateInfo.data[0].id
            let paymentID = await client.getAffiliatePaymentID(pId);
            console.log(paymentID)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('should get referrals payment status', async () => {
        try {
            let affiliateInfo = await client.getAffiliatePayment();
            console.log(affiliateInfo)
            let pId = affiliateInfo.data[0].id

            let paymentID = await client.getAffiliatePaymentIDStatus(pId);
            console.log(paymentID)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('payment create', async () => {
        try {
            let orderCreate = paymentOrder.paymentKey()
            let affiliateInfo = await client.postAffiliatePaymentCreate(
                {
                    gate: orderCreate.key.name,
                    options: {'address': orderCreate.key.address}
                }
            )
            console.log(affiliateInfo)

        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('user info', async () => {
        try {
            let affiliateInfo = await client.getUser();
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

})
