let assert = require('assert');

let baseUrl = require('../private/exchange/baseUrl')
let exchangeApi = require('../src/exchange/exchange_api')
let currency = require('../src/exchange/currency')
let order = require('../private/exchange/createOrder')


describe('Exchange', () => {
    let client = new exchangeApi.APIExchange(baseUrl.url)

    it('exchange limits', async () => {
        try {
            let pair = currency.randomPair()
            let affiliateInfo = await client.getExchangeLimits(pair.from.name, pair.to.name);
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('exchange limit pair', async () => {
        try {
            let pair = currency.randomPair()
            let pairStr = pair.from.symbol + '_' + pair.to.symbol
            console.log(pairStr)
            let affiliateInfo = await client.getExchangePairLimit(pairStr);
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('exchange list', async () => {
        try {
            let affiliateInfo = await client.getCurrenciesList();
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('create order', async () => {

        try {
            let curPair = order.exchangePair()
            let curPairStr = curPair.from.symbol + '_' + curPair.to.symbol
            console.log(curPairStr)
            let affiliateInfo = await client.postOrder(
                {
                    gate_depositjn: curPair.from.name,
                    gate_withdrawal: curPair.to.name,
                    pair: curPairStr,
                    deposit_amount: order.deposit_amount,
                    withdrawal_amount: order.withdrawal_amount,
                    email: order.email,
                    options: {'address': curPair.to.address}
                }
            );
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('rate', async () => {

        try {
            let affiliateInfo = await client.postRate(order);
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('validate address', async () => {

        try {
            let affiliateInfo = await client.postAddress(order);
            console.log(affiliateInfo)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });

    it('order status', async () => {
        try {
            let curPair = order.exchangePair()
            let curPairStr = curPair.from.symbol + '_' + curPair.to.symbol
            console.log(curPairStr)
            let affiliateInfo = await client.postOrder(
                {
                    gate_deposit: curPair.from.name,
                    gate_withdrawal: curPair.to.name,
                    pair: curPairStr,
                    deposit_amount: order.deposit_amount,
                    withdrawal_amount: order.withdrawal_amount,
                    email: order.email,
                    options: {'address': curPair.to.address}
                }
            )
            console.log(affiliateInfo)
            let secretKey = affiliateInfo.secret_key

            let orderStatusData = await client.getOrderStatus(secretKey);
            console.log(orderStatusData)
        } catch (e) {
            console.error(e)
            assert.equal(true, true)
        }
    });
})
