let names = {
    btc: {name: "bitcoin", symbol: "BTC"},
    ltc: {name: "litecoin", symbol: "LTC"},
    bch: {name: "bitcoincash", symbol: "BCH"},
    eth: {name: "ethereum", symbol: "ETH"},
    xlm: {name: "stellar", symbol: "XLM"},
    xrp: {name: "ripple", symbol: "XRP"},
    eos: {name: "eos", symbol: "EOS"}
}

function randomPair() {
    let keys = Object.keys(names)

    let from = names[keys[keys.length * Math.random() << 0]]
    let to = names[keys[keys.length * Math.random() << 0]]

    if (to === from) {
        to = names[keys[keys.length * Math.random() << 0]]
    }

    return {from, to}
}

module.exports = {
    btc: names.btc,
    ltc: names.ltc,
    bch: names.bch,
    eth: names.eth,
    xlm: names.xlm,
    xrp: names.xrp,
    eos: names.eos,
    randomPair
}
