const axios = require('axios');

let pathList = {
    getProfile: "/v1/profile",
    putProfile: "/v1/profile",
}

class APIProfile {
    constructor(baseURL) {
        this.baseURL = baseURL
    }

    headers() {
        return {
            "accept": "application/json",
            "content-type": "application/json",
        }
    }


}

module.exports.APIProfile = APIProfile
