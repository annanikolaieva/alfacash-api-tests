const axios = require('axios');

let pathList = {
    getAffiliate: "/api/v1/affiliate",
    getAffiliateLevels: "/api/v1/affiliate/levels",
    getAffiliateReferrals: "/api/v1/affiliate/referrals",
    getAffiliateIncome: "/api/v1/affiliate/income",
    getAffiliatePayment: "/api/v1/affiliate/payments",
    getAffiliatePaymentID: "/api/v1/affiliate/payments/",
    getAffiliatePaymentIDStatus: "/api/v1/affiliate/payments/",
    postAffiliatePaymentCreate: "/api/v1/affiliate/payments/create",
    getUser: "/api/v1/user",
    //  getUserLogout: "/api/v1/user/logout",

}

class APIClient {
    constructor(baseURL, token) {
        this.baseURL = baseURL
        this.token = 'Bearer ' + token
    }

    headers() {
        return {
            Authorization: this.token,
            "accept": "application/json",
            "content-type": "application/json",
        }
    }

    getAffiliate() {
        return axios.get(
            this.baseURL + pathList.getAffiliate,
            {headers: this.headers()},)
            .then(resp => {
                return resp.data
            })
    }


    getAffiliateLevels() {
        return axios.get(
            this.baseURL + pathList.getAffiliateLevels,
            {headers: this.headers()})
            .then(resp => {
                return resp.data
            })
    }

    getAffiliateReferrals(page, perPage) {
        return axios.get(
            this.baseURL + pathList.getAffiliateReferrals,
            {
                params: {page, perPage},
                headers: this.headers()
            })
            .then(resp => {
                return resp.data
            })
    }

    getAffiliateIncome(page, perPage) {
        return axios.get(
            this.baseURL + pathList.getAffiliateIncome,
            {
                headers: this.headers(),
                params: {page, perPage},
            })
            .then(resp => {
                return resp.data
            })
    }

    getAffiliatePayment(page, perPage) {
        return axios.get(
            this.baseURL + pathList.getAffiliatePayment,
            {
                headers: this.headers(),
                params: {page, perPage},
            })
            .then(resp => {
                return resp.data
            })
    }

    getAffiliatePaymentID(id) {
        return axios.get(
            this.baseURL + pathList.getAffiliatePaymentID + id,
            {
                headers: this.headers(),
            })
            .then(resp => {
                return resp.data
            })
    }

    getAffiliatePaymentIDStatus(id) {
        return axios.get(
            this.baseURL + pathList.getAffiliatePaymentIDStatus + id + '/status',
            {
                headers: this.headers(),
            })
            .then(resp => {
                return resp.data
            })
    }

    postAffiliatePaymentCreate(paymentOrder) {
        return axios.post(
            this.baseURL + pathList.postAffiliatePaymentCreate,
            {
                gate: paymentOrder.gate,
                options: paymentOrder.options
            },
            {headers: this.headers()})
            .then(resp => {
                return resp.data
            })
    }

    getUser() {
        return axios.get(
            this.baseURL + pathList.getUser,
            {
                headers: this.headers(),
            })
            .then(resp => {
                return resp.data
            })
    }
}

module.exports.APIClient = APIClient
