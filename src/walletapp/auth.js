const axios = require('axios');

let pathList = {
    postLogin: "/v1/auth/login",
    postRegister: "/v1/auth/register",
    postLoginCheck: "/v1/auth/login/check",
    getProfile: "/v1/profile",
    putProfile: "/v1/profile",

}

class APIAuth {

    constructor(baseURL) {
        this.baseURL = baseURL
        this.token=''
    }

    headers() {
        return {
            "accept": "application/json",
            "content-type": "application/json",
            "x-auth-token": this.token
        }
    }

    postLogin(login) {
        return axios.post(
            this.baseURL + pathList.postLogin,
            {
                address: login.address,
                signature: login.signature,
            },
            {headers: this.headers()})

            .then(resp => {
                console.log('fucking response:', resp.data)
                console.log('fucking:',resp.data.data.auth)
                this.token =  resp.data.data.auth.token
                console.log('token form API:',  resp.data.data.auth.token)
                console.log('my token:', this.token)
                })

    }


    postRegistration(login) {
        return axios.post(
            this.baseURL + pathList.postRegister,
            {
                address: login.address,
                signature: login.signature,
            },
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    postLoginCheck(login) {
        return axios.post(
            this.baseURL + pathList.postLoginCheck,
            {
                login: login.login,
            },
            {headers: this.headers()}
        ).then(resp => {
            return resp.data
        })
    }

    getProfile() {
        return axios.get(
            this.baseURL + pathList.getProfile,
            {headers: this.headers()})
            .then(resp => {
                    return resp.data
                }
            )
    }


    putProfile(login) {
        return axios.patch(
            this.baseURL + pathList.putProfile,
            {
                address: login.address,
                signature: login.signature,
            },
            {
                headers: this.headers(),
            }
        ).then(resp => {
            return resp.data
        })
    }

}

module.exports.APIAuth = APIAuth
